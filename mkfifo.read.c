#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <time.h>
#include <math.h>

/*
*
* method compare
*
* used to compare array items
*
*/
int compare(const void* a, const void* b){
     int int_a =*((int*)a);
     int int_b =*((int*)b);

     if(int_a==int_b) return 0;
     else if (int_a < int_b) return -1;
     else return 1;
}


int main(int argc, char**argv){
    int count;
    int item;
    int i;
    int *arr;
    int sum;
    float mean;
    float stddv;
    fscanf(stdin,"%d",&count);
    fprintf(stdout,"%d\n",count);
    arr=(int*)malloc(sizeof(int)*count);
    sum=0;
    for(i=0;i<count;i++){
        fscanf(stdin,"%d",&item);
        arr[i]=item;
        sum+=item;
    }
    mean=sum/(float)count;
    stddv=0;
    for(i=0;i<count;i++){ // find standard deviation
        stddv+=powf(arr[i]-mean,2.0);
    }
    stddv=sqrtf(stddv/(float)count);
    fprintf(stdout,"%f\n",stddv); // standard deviation
    qsort(arr, count, sizeof(int), compare);
    fprintf(stdout,"%d\n",arr[count/2]); // median value
    free(arr);
    return 0;
}
