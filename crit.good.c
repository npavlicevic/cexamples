#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>

int sum=0; // a shared variable, each thread can change this

pthread_mutex_t lock=PTHREAD_MUTEX_INITIALIZER; // a lock for each thread to use

/*
*
* method countGold
*
* count gold a method used by each thread
*
*/
void *countGold(void*param){
    int i; // local for each thread
    pthread_mutex_lock(&lock); // acquire a lock before going into critical section
    // any other thread that tries to acquire a lock must hold on
    // until a lock is released
    for(i=0;i<1000;i++){ // each thread count gold
        // only thread that acquired a lock can access this section
        sum=sum+1;
    }
    pthread_mutex_unlock(&lock); // release a lock
    return NULL;
}

int main(){
    pthread_t tid1,tid2;
    pthread_create(&tid1,NULL,countGold,NULL);
    pthread_create(&tid2,NULL,countGold,NULL);

    // hold on for both threads to finish
    pthread_join(tid1,NULL);
    pthread_join(tid2,NULL);

    fprintf(stdout, "sum is (good): %d\n", sum);

    return 0;
}
