#include <iostream>

const int track=0;

class Track {
    public:
    void move(){
        std::cout << "moving using track" << std::endl;
    }
};

class Vehicle {
    protected:
    void*mode; // mode used to move
    int type; // type of vehicle

    public:
    Vehicle(void*mode,int type){
        this->mode=mode;
        this->type=type;
    }
    void move(){
        if(this->type==track){
            ((Track*)(this->mode))->move();
        }
    }
};

int main(void) {
    // OUTPUT
    // moving using track
    Track*trck=new Track; 
    Vehicle*vhc = new Vehicle(trck,track);
    vhc->move();
    delete trck;
    delete vhc;
    return 0;
}
