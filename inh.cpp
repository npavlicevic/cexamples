#include <iostream>

class Vehicle {
    public:
    virtual void move(){
        std::cout << "moving" << std::endl;
    }
};

class Track : public Vehicle {
    public:
    virtual void move(){
        std::cout << "moving using track" << std::endl;
    }
};

int main(void) {
    // OUTPUT
    // moving using track 
    Vehicle*vhc = new Track;
    vhc->move();
    delete vhc;
    return 0;
}
