pointers.o: pointers.c
	gcc -c pointers.c
pointers: pointers.o
	gcc -o pointers pointers.o
volatile.o: volatile.c
	gcc -c volatile.c
volatile: volatile.o
	gcc -o volatile volatile.o
crit.bad.o: crit.bad.c
	gcc -c crit.bad.c
crit.bad: crit.bad.o
	gcc -o crit.bad crit.bad.o -pthread
crit.good.o: crit.good.c
	gcc -c crit.good.c
crit.good: crit.good.o
	gcc -o crit.good crit.good.o -pthread
mkfifo.o: mkfifo.c
	gcc -c mkfifo.c
mkfifo: mkfifo.o
	gcc -o mkfifo mkfifo.o
mkfifo.add.o: mkfifo.add.c
	gcc -c mkfifo.add.c
mkfifo.add: mkfifo.add.o
	gcc -o mkfifo.add mkfifo.add.o
mkfifo.read.o: mkfifo.read.c
	gcc -c mkfifo.read.c
mkfifo.read: mkfifo.read.o
	gcc -o mkfifo.read mkfifo.read.o -lm
tennis.o: tennis.c
	gcc -c tennis.c
tennis: tennis.o
	gcc -o tennis tennis.o
base.o: base.cpp
	g++ -c base.cpp
base: base.o
	g++ -o base base.o
inh.o: inh.cpp
	g++ -c inh.cpp
inh: inh.o
	g++ -o inh inh.o
inh.enc.o: inh.enc.cpp
	g++ -c inh.enc.cpp
inh.enc: inh.enc.o
	g++ -o inh.enc inh.enc.o
inh.poly.o: inh.poly.cpp
	g++ -c inh.poly.cpp
inh.poly: inh.poly.o
	g++ -o inh.poly inh.poly.o
