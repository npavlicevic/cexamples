#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

int main(int argc, char**argv){
    // make use of command line arguments
    // fifo name given as command line argument
    int result;
    if(argc != 2){ // need exactly 2 arguments
        fprintf(stderr,"Usage ./name filename\n");
        exit(1);
    }
    result = mknod(argv[1],S_IRUSR|S_IWUSR|S_IFIFO,0);
    if(result<0){
        fprintf(stderr, "could not make fifo\n");
        exit(2);
    }
    return 0;
}
