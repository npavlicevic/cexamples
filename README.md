C and CPP examples
------------------

A number of C and CPP examples. Also included an example repository organization.

Test environment
-----------------

OS Lubuntu 16.04 LTS 64 bit kernel version 4.13.0.  

Compile examples
----------------

- make pointers.o
- make pointers
- make volatile.o
- make volatile
- make crit.bad.o
- make crit.bad
- make crit.good.o
- make crit.good
- make mkfifo.o
- make mkfifo
- make mkfifo.add.o
- make mkfifo.add
- make mkfifo.read.o
- make mkfifo.read
- make tennis.o
- make tennis
- make base.o
- make base
- make inh.o
- make inh
- make inh.enc.o
- make inh.enc
- make inh.poly.o
- make inh.poly

Examples included
-----------------

- pointers (print variable value and variable address, run ./pointers)
- volatile (go through volatile concepts, run ./volatile)
- crit.bad (a bad example of multiple threads and critical section, run ./crit.bad, each run can yield different result)
- crit.good (a good example of multiple threads and critical section, run ./crit.good, each run yields same result)
- mkfifo (create fifo a named pipe, used for process communication, run ./mkfifo, check instructions)
- mkfifo.add (print random numbers to named pipe, run ./mkfifo.add, check instructions)
- mkfifo.read (read random numbers from named pipe, find median, find standard deviation, run ./mkfifo.read < namedpipe)
- tennis (a tennis game as finite state machine, run ./tennis, check instructions)
- base (a base class inheritance example, run ./base, check base.cpp for comments)
- inh (another class inheritance example, run ./inh, check inh.cpp for comments)
- inh.enc (encapsulation example, run ./inh.enc, check inh.enc.cpp for comments)
- inh.poly (polymorphism example, run ./inh.poly, check inh.poly.cpp for comments)
