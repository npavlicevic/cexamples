#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <time.h>

int main(int argc, char**argv){
    // make use of command line arguments
    // fifo name given as command line argument
    // sleep time given as command line argument
    // count given as command line argument
    int result;
    int i;
    float sleeptime;
    int count;
    int max;
    time_t t;
    if(argc != 4){ // need exactly 4 arguments
        fprintf(stderr,"Usage ./name sleeptime count max\n");
        fprintf(stderr,"Usage (or) ./name sleeptime count max > namedpipe &\n");
        exit(1);
    }
    sleeptime=atof(argv[1]);
    count=atoi(argv[2]);
    max=atoi(argv[3]);
    srand((unsigned)time(&t));
    fprintf(stdout,"%d\n",count);
    for(i=0;i<count;i++){
        fprintf(stdout,"%d\n",rand()%max);
        sleep(sleeptime);
    }
    return 0;
}
