#include <iostream>

class Base {
    virtual void method() {std::cout << "from Base" << std::endl;}
    public:
    virtual ~Base() {method();}
    void baseMethod() {method();}
};

class A : public Base {
    void method() {std::cout << "from A" << std::endl;}
    public:
    ~A() {method();}
};

int main(void) {
    // OUTPUT
    // from A 
    // from A
    // from Base
    // call baseMethod method overriden
    // call destructor A method overriden
    // call destructor Base method not overriden
    Base* base = new A;
    base->baseMethod();
    delete base;
    return 0;
}
