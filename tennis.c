#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <unistd.h>

typedef struct player {
    int score;
} Player; // a tennis player

int gameScore=0; // current game score
int advantage; // determine advantage, player a or player b
float sleeptime; // thread sleep time
Player playera;
Player playerb;
const int gainScore=2; // score difference needed for a game
int gainPlayer; // player taking a game

void playeraRun(){ // check player a
    if(advantage==0){
        playera.score++;
    }
}
void playerbRun(){ // check player b
    if(advantage==1){
        playerb.score++;
    }
}
void generate(){ // decide point gain next tick
    advantage=rand()%2;
    // 0 player a advantage
    // 1 player b advantage
}
void checkScore(){ // check if game done
    int lscore=playera.score-playerb.score;
    gameScore=abs(lscore);

    if(gameScore>=gainScore){ // game decided
        if(lscore>0){
            gainPlayer=0;
        } else {
            gainPlayer=1;
        }
    }
}
void printScore(){ // print current score
    fprintf(stdout,"score %d %d\n", playera.score, playerb.score);
}

int main(int argc, char**argv){
    time_t t;
    srand((unsigned)time(&t));
    playera.score=0;
    playerb.score=0;
    if(argc != 2){ // need exactly 2 arguments
        fprintf(stderr,"Usage ./name sleeptime\n");
        exit(1);
    }
    sleeptime=atof(argv[1]);

    for(;gameScore<gainScore;){ // keep running the game
        sleep(sleeptime);
        generate();
        playeraRun();
        playerbRun();
        printScore();
        checkScore();
    }

    fprintf(stdout,"player to take game: %d\n", gainPlayer);

    return 0;
}
