#include <stdlib.h>
#include <stdio.h>

typedef struct gadget {
    int command;
    int data;
    int isbusy;
} Gadget;

/*
*
* method sendCommand
*
* send command to a gadget
*
* parameters
*
* gadget (volatile) a gadget to use
* 
*  declare as volatile, do not cache values change values as needed
*
* command command code 
*
* data data to use
*
*/
void sendCommand(volatile Gadget*gadget, int command, int data){
    for(;gadget->isbusy;){
        // hold on gadget is busy
    }

    gadget->command=command;
    gadget->data=data;
}

/*
*
* method init
*
* initialize a gadget
*
* parameters
*
* gadget (volatile) a gadget to use
* 
*  declare as volatile, do not cache values change values as needed
*
* command command code 
*
* data data to use
*
* isbusy isbusy gadget status
*
*/
void init(volatile Gadget*gadget, int command, int data, int isbusy){
    gadget->command=command;
    gadget->data=data;
    gadget->isbusy=isbusy;
}

int main(){
    Gadget*gadget=(Gadget*)malloc(sizeof(Gadget));
    int command=3;
    int data=5;
    int isbusy=0;
    init(gadget,command,data,isbusy);
    sendCommand(gadget,command,data);
    free(gadget);
    return 0;
}
