#include <stdlib.h>
#include <stdio.h>

int main(){
    int vr = 2016;
    int *vrpta = &vr;
    int **vrptb = &vrpta;
    int ***vrptc = &vrptb;
    fprintf(stdout,"vr %d\n",vr);
    fprintf(stdout,"&vr %p\n", &vr);
    fprintf(stdout,"*vrpta %d\n", *vrpta);
    fprintf(stdout,"**vrptb %d\n", **vrptb);
    fprintf(stdout,"***vrptc %d\n", ***vrptc);
    return 0;
}
