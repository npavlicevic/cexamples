#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>

int sum=0; // a shared variable, each thread can change this

/*
*
* method countGold
*
* count gold a method used by each thread
*
*/
void *countGold(void*param){
    int i; // local for each thread
    for(i=0;i<1000;i++){ // each thread count gold
        // each thread has access to critical section
        // access by each thread is not regulated
        // any thread can access value and change it at any time
        // the result is highly unpredictable value
        sum=sum+1;
    }
    return NULL;
}

int main(){
    pthread_t tid1,tid2;
    pthread_create(&tid1,NULL,countGold,NULL);
    pthread_create(&tid2,NULL,countGold,NULL);

    // hold on for both threads to finish
    pthread_join(tid1,NULL);
    pthread_join(tid2,NULL);

    fprintf(stdout, "sum is (bad): %d\n", sum);

    return 0;
}
