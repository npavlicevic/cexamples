#include <iostream>

class Vehicle {
    public:
    virtual void move(){
        std::cout << "moving" << std::endl;
    }
};

class Ice:public Vehicle { // internal combustion engine
    public:
    virtual void move(){
        std::cout << "moving using ice" << std::endl;
    }
};

class Electric:public Vehicle { // move using batteries
    public:
    virtual void move(){
        std::cout << "moving using batteries" << std::endl;
    }
};

int main(void) {
    // OUTPUT
    // moving using ice 
    // moving using batteries 
    Vehicle*vhca = new Ice;
    Vehicle*vhcb = new Electric;
    vhca->move();
    vhcb->move();
    delete vhca;
    delete vhcb;
    return 0;
}
